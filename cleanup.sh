#!/bin/env bash

rm -r DotNetWorker
sudo systemctl disable dotnet-worker
sudo systemctl stop dotnet-worker
sudo rm /etc/systemd/system/dotnet-worker.service
sudo rm -r /opt/dotnet-worker
sudo rm -r /var/log/dotnet-worker
sudo systemctl daemon-reload

