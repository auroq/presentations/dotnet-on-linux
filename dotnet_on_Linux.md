%title: .NET Apps on Linux
%author: Parker Johansen
%date: 2020-02-10

-> # .NET Apps On Linux <-

-> The future is now! <-

---

-> # Three Parts to that Linux Life <-

- Developing on Linux
- Deploying to Linux
- Monitoring/Debugging on Linux

---

-> # Developing on Linux <-

# Toolsets

- Rider
- Vim
- VSCode
- Atom
- That competitor to vim that we don't talk about
- Nano, I guess

\#novimnocode

---

-> # Developing on Linux <-

# Linux Flavors of .NET
- Mono
- .NET Core
- .NET Standard

---

-> # Developing on Linux <-

# Mono
- .NET Framework implementation
- Cross platform
- Open Source
- Sponsored by Microsoft

---

-> # Developing on Linux <-

# Dotnet Core
- Software framework developed by Microsoft
- “Built on Adaptability”
  - Cross platform
  - Microservices
  - Packages
  - Extension methods
- ASP.NET Core
  - Web Framework
  - Replaced ASP.NET

---

-> # Developing on Linux <-

- Software framework developed by Microsoft
- Bridge between Core and Framework

---

-> # Developing on Linux <-

# CLI Tools

| Command        | Description                                                       |
| :------------- | :---------------------------------------------------------------- |
| dotnet new     | Create a new .NET project or file.                                |
| dotnet build   | Build a .NET project.                                             |
| dotnet run     | Build and run a .NET project.                                     |
| dotnet test    | Run unit tests using the test runner specified in a .NET project. |
| dotnet nuget   | Provides additional NuGet commands.                               |
| dotnet publish | Publish a .NET project for deployment.                            |
| dotnet DLL     | Run a specific compiled dll.                                      |

---

-> # Developing on Linux <-

# More CLI Tools

| Command/Tool                 | Description                                                                   |
| :--------------------------- | :---------------------------------------------------------------------------- |
| mkdir [-p] DIRECTORY         | Create the DIRECTORY(ies), if they do not already exist.                      |
| ln -s TARGET DIRECTORY       | Create a symbolic link to TARGET in DIRECTORY                                 |
| tail -f FILE                 | Print the last 10 lines of a file and continue printing as lines are appended |
| export VAR=VAL               | Create a variable "VAR" in the current session with value "VAL"               |
| dotnet-install.sh            | Installer/manager for dotnet sdks/runtimes                                    |
| dos2unix FILE                | Converts windows line endings to unix line endings                            |
| chmod [OPTION] FILE          | Change the mode of each FILE to MODE (read: 4, write: 2, execute: 1)          |
| chown [OWNER]\[:[GROUP]] FILE | Change the owner and/or group of each FILE to OWNER and/or GROUP.             |
| cat FILE                     | Concatenate FILE(s) to standard output.                                       |
| cd DIR                       | Change the current directory to DIR.                                          |

---

-> # Developing on Linux <-

# Live Demo

\#bash \#vim \#novimnocode

---

-> # Deploying to Linux <-

# Similarities

- Publish the app
- Copy files to the server
- Run the app as a service

---

-> # Deploying to Linux <-

# Differences

- Service Manager
  - Systemd
- Web Server
  - Kestrel
  - Apache
  - Nginx

---

-> # Deploying to Linux <-

# systemd

- System and service management suite for linux
- Runs as PID 1 and starts the rest of the system

---

-> # Deploying to Linux <-

# systemd commands

*systemctl: Query or send control commands to the system manager*

| Systemctl Command | Description                                            |
| :---------------- | :----------------------------------------------------- |
| status [UNIT]     | Show Runtime status of one or more units               |
| start UNIT        | Start (activate) one or more units                     |
| stop UNIT         | Stop (deactivate) one or more units                    |
| enable UNIT       | Enable one or more unit files (to start automatically) |
| disable UNIT      | Disable one or more unit files                         |
| daemon-reload     | Reload systemd manager configuration                   |

---

-> # Deploying to Linux <-

# systemd unit files

- A Plain test ini-style file that encodes information about a service (or other systemd unit types)

## .NET example:
```sh
[Unit]
Description=Dotnet Worker Demo

[Service]
ExecStart=/usr/bin/dotnet DotnetWorker.dll
Restart=on-failure
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=dotnet-worker
WorkingDirectory=/opt/DotnetWorker/
KillSignal=SIGINT

[Install]
WantedBy=multi-user.target
```

---

-> # Deploying to Linux <-

# Live Demo

\#bash \#systemd

---

-> # Monitoring/Debugging Deployed Apps on Linux <-

# journalctl

*"Query the journal"*

| journalctl Flags | Description                                              |
| :----------------- | :----------------------------------------------------- |
| -u UNIT|PATTERN    | Show logs from unit or units matching the pattern      |
| -f                 | Follow the journal                                     |
| -b [ID]            | Show current or specified boot                         |
| -g PATTERN         | Show entries with MESSAGE matching PATTERN (grep)      |


---

-> # Monitoring/Debugging Deployed Apps on Linux <-

# Raw Live Demo

\#bash \#systemd \#journalctl \#watch \#tail \#varlog

---

-> # Questions? <-

# Resources

- .NET
  - [.NET cli reference](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet?tabs=netcore21)
  - [.NET Core and systemd: Glenn Condron](https://devblogs.microsoft.com/dotnet/net-core-and-systemd/)
  - [About .NET Core](https://docs.microsoft.com/en-us/dotnet/core/about)
  - [About .NET Standard](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
  - [Mono](https://www.mono-project.com/)
  - [Arch Wiki: .NET Core](https://wiki.archlinux.org/index.php/.NET_Core)
- Web Servers
  - [Kestrel](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/servers/kestrel?view=aspnetcore-3.1)
  - [Apache](https://www.apache.org/)
  - [NGINX](https://www.nginx.com/)
- systemd
  - [systemd](https://www.freedesktop.org/wiki/Software/systemd/)
  - [systemctl](https://www.freedesktop.org/software/systemd/man/systemctl.html)
  - [journalctl](https://www.freedesktop.org/software/systemd/man/journalctl.html)
  - [systemd.unit](https://www.freedesktop.org/software/systemd/man/systemd.unit.html)
  - [Arch Wiki: systemd](https://wiki.archlinux.org/index.php/systemd)
- Vim and Vim Plugins
  - [vim](https://www.vim.org/)
  - [IdeaVim (Jetbrains)](https://plugins.jetbrains.com/plugin/164-ideavim)
  - [VsVim (Visual Studio)](https://marketplace.visualstudio.com/items?itemName=JaredParMSFT.VsVim)
  - [vscodevim (VS Code)](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim)
  - [vim-mode-plux (atom)](https://atom.io/packages/vim-mode-plus)
  - [Evil (that competitor to vim that we don't talk about)](https://www.emacswiki.org/emacs/Evil)
- [MDP (Presentation Tool)](https://github.com/visit1985/mdp)

