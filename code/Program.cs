using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace DotNetWorker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var builtConfig = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.File(builtConfig["Logging:FilePath"] ?? "worker.log")
                .WriteTo.Console()
                .CreateLogger();
            Log.Logger = logger;

            try
            {
                return Host.CreateDefaultBuilder(args)
                    .ConfigureAppConfiguration((hostingContext, config) => config.AddConfiguration(builtConfig))
                    .ConfigureLogging(logging => logging.AddSerilog())
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddSingleton<Serilog.ILogger>(logger);
                        services.AddHostedService<Worker>();
                    });
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host builder error");

                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
