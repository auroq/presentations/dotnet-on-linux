#!/bin/env bash

### INITIALIZE DEMO ###
. demo-magic.sh $1

clear

#######################

pe "mkdir -p DotNetWorker/DotNetWorker"
pe "cd DotNetWorker"
pe "dotnet new sln"
pe "ls -alF"
pe "cat DotNetWorker.sln"
pe "cd DotNetWorker"
pe "dotnet new worker"
pe "ls -alF"
pe "cd .."
pe "dotnet sln add DotNetWorker"
pe "cat DotNetWorker.sln"
pe "cd .."
pe "dos2unix */*/*"
pe "cd DotNetWorker/DotNetWorker"
pe "dotnet build"
pe "dotnet run"

#####

pe "dotnet add package Microsoft.Extensions.Hosting"
pe "dotnet add package Microsoft.Extensions.Hosting.Systemd"
pe "dotnet add package Serilog.AspNetCore"
pe "dotnet add package Serilog.Sinks.File"
pe "cat DotNetWorker.csproj"
p  "vim Program.cs"
clear
cat Program.cs
cp ../../code/Program.cs ./Program.cs
PROMPT_TIMEOUT=1
wait
clear
cat Program.cs
PROMPT_TIMEOUT=0
p  ":wq"
PROMPT_TIMEOUT=1
wait
clear
PROMPT_TIMEOUT=0
p  "vim Worker.cs"
clear
cat Worker.cs
cp ../../code/Worker.cs ./Worker.cs
PROMPT_TIMEOUT=1
wait
clear
cat Worker.cs
PROMPT_TIMEOUT=0
p  ":wq"
PROMPT_TIMEOUT=1
wait
clear
PROMPT_TIMEOUT=0
pe "dotnet build"
pe "dotnet run"

p  "vim appsettings.json"
clear
cat appsettings.json
cp ../../code/appsettings.json appsettings.json
PROMPT_TIMEOUT=1
wait
clear
cat appsettings.json
PROMPT_TIMEOUT=0
p  ":wq"
PROMPT_TIMEOUT=1
wait
clear
PROMPT_TIMEOUT=0

pe "dotnet publish -c Release -o output"
pe "ls -alF output"

pe "sudo install -dm755 /opt/dotnet-worker"
pe "sudo install -dm755 /var/log/dotnet-worker"
pe "sudo cp -ar output/* /opt/dotnet-worker"
pe "ls -alF /opt/dotnet-worker"

pe "cat > dotnet-worker.service << EOF

[Unit]
Description=DotNet Worker Demo

[Service]
ExecStart=/usr/bin/dotnet DotNetWorker.dll
Restart=on-failure
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=dotnet-worker
WorkingDirectory=/opt/dotnet-worker/
KillSignal=SIGINT

[Install]
WantedBy=multi-user.target

EOF"

pe "sudo install -Dm755 dotnet-worker.service /etc/systemd/system/dotnet-worker.service"

pe "sudo systemctl daemon-reload"
pe "sudo systemctl status dotnet-worker"
pe "sudo systemctl enable dotnet-worker"
pe "sudo systemctl status dotnet-worker"
pe "sudo systemctl start dotnet-worker"
pe "sudo systemctl status dotnet-worker"

